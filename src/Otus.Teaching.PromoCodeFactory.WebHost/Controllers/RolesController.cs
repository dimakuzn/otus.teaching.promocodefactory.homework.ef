﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="500">API error</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<RoleItemResponse>), (int)StatusCodes.Status200OK)]
        public async Task<IEnumerable<RoleItemResponse>> GetRolesAsync()
        {
            IEnumerable<Role> roles = await _rolesRepository.GetAllAsync();

            return roles.Select(x =>
                new RoleItemResponse(x)).ToList();
        }
    }
}