﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Role> _roleRepository;

        public PromocodesController(
            IRepository<Customer> repo1,
            IRepository<PromoCode> repo2,
            IRepository<Employee> repo3,
            IRepository<Preference> repo4,
            IRepository<Role> repo5)
        {
            _customerRepository = repo1;
            _promoCodeRepository = repo2;
            _employeeRepository = repo3;
            _preferenceRepository = repo4;
            _roleRepository = repo5;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            IEnumerable<PromoCode> codes = await _promoCodeRepository.GetAllAsync();

            return codes.Select(c => new PromoCodeShortResponse(c)).ToList();
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            IEnumerable<Customer> customers = await _customerRepository.GetAllAsync();
            List<Customer> customersWithPreference =
                customers.Where(cst => cst.Preferences.Any(pr => pr.Name == request.Preference)).ToList();

            IEnumerable<Employee> employees = await _employeeRepository.GetAllAsync();


            IEnumerable<Role> roles = await _roleRepository.GetAllAsync();
            Role partnerRole = roles.Where(r => r.Name == "PartnerManager").FirstOrDefault();
            if (partnerRole == null)
            {
                return NotFound("Partner role not found");
            }

            Employee partnerManager = employees.Where(prt => prt.RoleId == partnerRole.Id).FirstOrDefault();
            if (partnerManager == null)
            {
                return NotFound("Partner manager not found");
            }

            IEnumerable<Preference> preferences = await _preferenceRepository.GetAllAsync();
            Preference preference = preferences.Where(prt => prt.Name == request.Preference).FirstOrDefault();

            if (preference == null)
            {
                return NotFound("Preference not found");
            }

            customersWithPreference.ForEach(cst =>
            {
                PromoCode promo = new PromoCode()
                {
                    Id = new Guid(),
                    Code = request.Code,
                    ServiceInfo = request.ServiceInfo,
                    BeginDate = new DateTime(),
                    EndDate = new DateTime().AddDays(7),
                    PartnerName = request.PartnerName,
                    PartnerManagerId = partnerManager.Id,
                    PreferenceId = preference.Id,
                    CustomerId = cst.Id
                };
                _promoCodeRepository.Create(promo);
            });

            return Ok();
        }
    }
}