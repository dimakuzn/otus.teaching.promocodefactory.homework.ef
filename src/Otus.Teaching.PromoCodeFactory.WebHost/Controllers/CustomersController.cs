﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoRepository;


        public CustomersController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCode> promoRepository
            
            )
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoRepository = promoRepository;
        }


        /// <summary>
        /// Получить всех заказчиков
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="500">API error</response>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            IEnumerable<Customer> customers = await _customerRepository.GetAllAsync();
            List<CustomerShortResponse> res = customers.Select(x => new CustomerShortResponse(x)).ToList();

            return res;
        }

        /// <summary>
        /// Получить данные заказчика по Id
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="500">API error</response>
        /// 
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            CustomerResponse model = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.Preferences.Select(pref => new PreferenceResponse(pref)).ToList(),

                PromoCodes = customer.PromoCodes != null
                    ? customer.PromoCodes.Select(code => new PromoCodeShortResponse(code)).ToList()
                    : new List<PromoCodeShortResponse>()
            };

            return model;
        }
        
        /// <summary>
        /// Создать заказчика
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="500">API error</response>

        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            IEnumerable<Preference> preferences = await _preferenceRepository.GetAllAsync();
            List<Preference> preferencesFiltered = preferences.Where(prt => request.PreferenceIds.Contains(prt.Id)).ToList();


            Customer res = new Customer()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = preferencesFiltered
            };

            _customerRepository.Create(res);

            return Ok();
        }

        /// <summary>
        /// Обновить данные заказчика
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="500">API error</response>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound("Customer not found");
            }

            IEnumerable<Preference> preferences = await _preferenceRepository.GetAllAsync();
            List<Preference> preferencesFiltered =
                preferences.Where(prt => request.PreferenceIds.Contains(prt.Id)).ToList();

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Preferences = preferencesFiltered;

            _customerRepository.Update(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить заказчика и его промокоды
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="500">API error</response>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound("Customer not found");
            }

            foreach (var promo in customer.PromoCodes.ToList())
            {
                _promoRepository.Remove(promo);
            }
            
            _customerRepository.Remove(customer);
            return Ok();
        }
    }
}