﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public PreferenceResponse(Preference pr)
        {
            this.Id = pr.Id;
            this.Name = pr.Name;
            
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}