﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleItemResponse
    {
        public RoleItemResponse(Role role)
        {
            Id = role.Id;
            Name = role.Name;
            Description = role.Description;
        }
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
    }
}