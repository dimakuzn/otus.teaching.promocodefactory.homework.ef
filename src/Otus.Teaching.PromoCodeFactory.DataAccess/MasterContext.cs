﻿
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
	public class MasterContext : DbContext
	{
        public MasterContext(DbContextOptions<MasterContext> options): base(options)
		{
        }
        
        public virtual DbSet<Employee> Employee { get; set; }

		public virtual DbSet<Role> Role { get; set; }

        public virtual DbSet<Customer> Customer { get; set; }

        public virtual DbSet<Preference> Preference { get; set; }

        public virtual DbSet<PromoCode> PromoCode { get; set; }

    }
}

