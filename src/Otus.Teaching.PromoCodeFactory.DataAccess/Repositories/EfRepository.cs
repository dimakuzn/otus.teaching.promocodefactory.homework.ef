﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbSet;

        public EfRepository(MasterContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> Create(T item)
        {
            await _dbSet.AddAsync(item);
            _context.SaveChangesAsync();

            return item;
        }
        
        public async Task<T> Update(T item)
        {
            _dbSet.Update(item);
            await _context.SaveChangesAsync();

            return item;
        }
        
        public async Task<T> Remove(T item)
        {
            _dbSet.Remove(item);
            await _context.SaveChangesAsync();

            return item;
        }
        
    }
}