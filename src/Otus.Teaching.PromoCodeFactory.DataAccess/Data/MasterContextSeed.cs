﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
	public class MasterContextSeed
	{
		MasterContext _context;
		public MasterContextSeed(MasterContext db)
		{
			_context = db;
		}

		public void Run()
		{
            using (MasterContext ctx = _context)
            {
                ctx.Database.EnsureDeleted();
                ctx.Database.EnsureCreated();


                _context.AddRange(FakeDataFactory.Employees);
                _context.SaveChanges();

                /*_context.AddRange(FakeDataFactory.Preferences);
                _context.SaveChanges();*/

                _context.AddRange(FakeDataFactory.Customers);
                _context.SaveChanges();
            }
        }
            
	}
}

